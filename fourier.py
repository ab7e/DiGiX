#
# Copyright 2020 Scott Eric Gilbert.  All Rights Reserved.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# This Source Code Form is "Incompatible With Secondary Licenses", as
# defined by the Mozilla Public License, v. 2.0.
#

from cmath import exp, pi
from cffi import FFI
_ffi = FFI()

def _transpose(buf, count, rows, cols):
    temp = range(rows*cols)
    offset = 0
    for cc in range(count):
        for ii in range(rows):
            for jj in range(cols):
                temp[jj*rows + ii] = buf[offset + ii*cols + jj]
        for ij in range(rows*cols):
            buf[offset + ij] = temp[ij]
        offset += rows*cols

def _radix2(buf, offset, count, cols, twid):
    off0, off1 = range(
        offset, offset + 2*cols, cols
    )
    step = 2*cols - cols
    for cc in range(count):
        index = stride = count
        for jj in range(1, cols):
            tt = twid[index]
            index += stride
            buf[off1 + jj] *= tt
        for jj in range(cols):
            z0 = buf[off0]
            z1 = buf[off1]
            buf[off0] = z0 + z1
            buf[off1] = z0 - z1
            off0 += 1; off1 += 1
        off0 += step; off1 += step

def _radix3(buf, offset, count, cols, twid):
    w1 = twid[count*cols]
    w2 = w1.conjugate()
    off0, off1, off2 = range(
        offset, offset + 3*cols, cols
    )
    step = 3*cols - cols
    for cc in range(count):
        rowoff = off0
        for ii in range(1, 3):
            rowoff += cols
            index = stride = count*ii
            for jj in range(1, cols):
                tt = twid[index]
                index += stride
                buf[rowoff + jj] *= tt
        for jj in range(cols):
            z0 = buf[off0]
            z1 = buf[off1]
            z2 = buf[off2]
            buf[off0] = z0 +    z1 +    z2
            buf[off1] = z0 + w1*z1 + w2*z2
            buf[off2] = z0 + w2*z1 + w1*z2
            off0 += 1; off1 += 1; off2 += 1
        off0 += step; off1 += step; off2 += step

def _radix4(buf, offset, count, cols, twid):
    w1 = twid[count*cols]
    off0, off1, off2, off3 = range(
        offset, offset + 4*cols, cols
    )
    step = 4*cols - cols
    for cc in range(count):
        rowoff = off0
        for ii in range(1, 4):
            rowoff += cols
            index = stride = count*ii
            for jj in range(1, cols):
                tt = twid[index]
                index += stride
                buf[rowoff + jj] *= tt
        for jj in range(cols):
            z0 = buf[off0]
            z1 = buf[off1]
            z2 = buf[off2]
            z3 = buf[off3]
            s02, d02 = z0 + z2, z0 - z2
            s13, w1d13 = z1 + z3, w1*(z1 - z3)
            buf[off0] = s02 +   s13
            buf[off1] = d02 + w1d13
            buf[off2] = s02 -   s13
            buf[off3] = d02 - w1d13
            off0 += 1; off1 += 1; off2 += 1; off3 += 1
        off0 += step; off1 += step
        off2 += step; off3 += step

def _radix5(buf, offset, count, cols, twid):
    w1 = twid[count*cols]
    w2 = twid[2*count*cols]
    w3 = w2.conjugate()
    w4 = w1.conjugate()
    off0, off1, off2, off3, off4 = range(
        offset, offset + 5*cols, cols
    )
    step = 5*cols - cols
    for cc in range(count):
        rowoff = off0
        for ii in range(1, 5):
            rowoff += cols
            index = stride = count*ii
            for jj in range(1, cols):
                tt = twid[index]
                index += stride
                buf[rowoff + jj] *= tt
        for jj in range(cols):
            z0 = buf[off0]
            z1 = buf[off1]
            z2 = buf[off2]
            z3 = buf[off3]
            z4 = buf[off4]
            buf[off0] = z0 +    z1 +    z2 +    z3 +    z4
            buf[off1] = z0 + w1*z1 + w2*z2 + w3*z3 + w4*z4
            buf[off2] = z0 + w2*z1 + w4*z2 + w1*z3 + w3*z4
            buf[off3] = z0 + w3*z1 + w1*z2 + w4*z3 + w2*z4
            buf[off4] = z0 + w4*z1 + w3*z2 + w2*z3 + w1*z4
            off0 += 1; off1 += 1; off2 += 1; off3 += 1; off4 += 1
        off0 += step; off1 += step; off2 += step
        off3 += step; off4 += step;

def _radix7(buf, offset, count, cols, twid):
    w1 = twid[count*cols]
    w2 = twid[2*count*cols]
    w3 = twid[3*count*cols]
    w4 = w3.conjugate()
    w5 = w2.conjugate()
    w6 = w1.conjugate()
    off0, off1, off2, off3, off4, off5, off6 = range(
        offset, offset + 7*cols, cols
    )
    step = 7*cols - cols
    for cc in range(count):
        rowoff = off0
        for ii in range(1, 7):
            rowoff += cols
            index = stride = count*ii
            for jj in range(1, cols):
                tt = twid[index]
                index += stride
                buf[rowoff + jj] *= tt
        for jj in range(cols):
            z0 = buf[off0]
            z1 = buf[off1]
            z2 = buf[off2]
            z3 = buf[off3]
            z4 = buf[off4]
            z5 = buf[off5]
            z6 = buf[off6]
            buf[off0] = z0 +    z1 +    z2 +    z3 +    z4 +    z5 +    z6
            buf[off1] = z0 + w1*z1 + w2*z2 + w3*z3 + w4*z4 + w5*z5 + w6*z6
            buf[off2] = z0 + w2*z1 + w4*z2 + w6*z3 + w1*z4 + w3*z5 + w5*z6
            buf[off3] = z0 + w3*z1 + w6*z2 + w2*z3 + w5*z4 + w1*z5 + w4*z6
            buf[off4] = z0 + w4*z1 + w1*z2 + w5*z3 + w2*z4 + w6*z5 + w3*z6
            buf[off5] = z0 + w5*z1 + w3*z2 + w1*z3 + w6*z4 + w4*z5 + w2*z6
            buf[off6] = z0 + w6*z1 + w5*z2 + w4*z3 + w3*z4 + w2*z5 + w1*z6
            off0 += 1; off1 += 1; off2 += 1; off3 += 1
            off4 += 1; off5 += 1; off6 += 1
        off0 += step; off1 += step; off2 += step; off3 += step
        off4 += step; off5 += step; off6 += step

def _radix8(buf, offset, count, cols, twid):
    w1 = twid[1*count*cols]
    w2 = twid[2*count*cols]
    w3 = twid[3*count*cols]
    w4 = twid[4*count*cols]
    off0, off1, off2, off3, off4, off5, off6, off7 = range(
        offset, offset + 8*cols, cols
    )
    step = 8*cols - cols
    for cc in range(count):
        rowoff = off0
        for ii in range(1, 8):
            rowoff += cols
            index = stride = count*ii
            for jj in range(1, cols):
                tt = twid[index]
                index += stride
                buf[rowoff + jj] *= tt
        for jj in range(cols):
            z0, z1 = buf[off0], buf[off1]
            z2, z3 = buf[off2], buf[off3]
            z4, z5 = buf[off4], buf[off5]
            z6, z7 = buf[off6], buf[off7]
            s04, d04, s15, d15 = z0 + z4, z0 - z4, z1 + z5, z1 - z5
            s26, d26, s37, d37 = z2 + z6, z2 - z6, z3 + z7, z3 - z7
            buf[off0] = s04 +    s15 +    s26 +    s37
            buf[off1] = d04 + w1*d15 + w2*d26 + w3*d37
            buf[off2] = s04 + w2*s15 -    s26 - w2*s37
            buf[off3] = d04 + w3*d15 - w2*d26 + w1*d37
            buf[off4] = s04 -    s15 +    s26 -    s37
            buf[off5] = d04 - w1*d15 + w2*d26 - w3*d37
            buf[off6] = s04 - w2*s15 -    s26 + w2*s37
            buf[off7] = d04 - w3*d15 - w2*d26 - w1*d37
            off0 += 1; off1 += 1; off2 += 1; off3 += 1;
            off4 += 1; off5 += 1; off6 += 1; off7 += 1;
        off0 += step; off1 += step; off2 += step; off3 += step
        off4 += step; off5 += step; off6 += step; off7 += step

def _factor(size):
    "Factor size into our known radixes and what's left"
    result = []
    for radix in [8, 4, 2, 3, 5, 7]:
        while size%radix == 0:
            result.append(radix)
            size /= radix
    if size > 1: result.append(size)
    result.sort()
    return result

def _memoize(f):
    cache = {}
    def wrapper(*args):
        if args not in cache:
            cache[args] = f(*args)
        return cache[args]
    return wrapper

@_memoize
def _fourstep(size, sign):
    "Build a Cooley-Tukey Four Step FFT"
    perm = _ffi.new('size_t[]', size)
    twid = _ffi.new('double _Complex[]', size)
    root = sign*2.0*pi*1j/size
    for ii in range(size):
        perm[ii] = ii
        twid[ii] = exp(ii*root)

    count = 1
    cols = size
    dfts = {
        2: _radix2, 3: _radix3, 4: _radix4,
        5: _radix5, 7: _radix7, 8: _radix8
    }
    factors = _factor(size)
    ops = []
    for radix in factors:
        cols /= radix
        dft = dfts.get(radix)
        if dft is None:
            assert cols == 1, "Bluestein must be last"
            dft = _bluestein(radix, sign)
            ops.append(
                lambda buf, off, dft=dft, count=count: (
                    dft(buf, off, count)
                )
            )
        else:
            _transpose(perm, count, cols, radix)
            ops.append(
                lambda buf, off, dft=dft, count=count, cols=cols: (
                    dft(buf, off, count, cols, twid)
                )
            )
        count *= radix
    ops = ops[::-1]

    def function(dst, dstoff, src, srcoff, srcstep):
        if dstoff == 0 and srcoff == 0 and srcstep == 1:
            for ii in range(size):
                dst[ii] = src[perm[ii]]
        else:
            for ii in range(size):
                dst[dstoff + ii] = src[
                    srcoff + perm[ii]*srcstep
                ]
        for op in ops: op(dst, dstoff)
    return function
        
@_memoize
def _bluestein(size, sign):
    "Build a Bluestein Chirp-Z FFT"
    over = 4
    while over < 2*size - 2:
        over *=2
    if 3*over/4 >= 2*size - 2:
        over = 3*over/4
    fwdfft = _fourstep(over, +sign)
    invfft = _fourstep(over, -sign)
    chirp = _ffi.new('double _Complex[]', size)
    cdata = _ffi.new('double _Complex[]', over)
    tdata = _ffi.new('double _Complex[]', over)
    fdata = _ffi.new('double _Complex[]', over)
    halfu = sign*pi*1j/size
    for ii in range(0, size):
        chirp[ii] = exp(halfu*ii*ii)
    tdata[0] = 1+0j
    for ii in range(1, size):
        zz = chirp[ii].conjugate()
        tdata[ii] = zz
        tdata[over - ii] = zz
    fwdfft(cdata, 0, tdata, 0, 1)
    scale = over**-.5
    for ii in range(size):
        chirp[ii] *= scale

    def function(buf, offset, count):
        for cc in range(count):
            for ii in range(size):
                tdata[ii] = buf[offset + ii]*chirp[ii]
            for ii in range(size, over):
                tdata[ii] = 0+0j
            fwdfft(fdata, 0, tdata, 0, 1)
            for ii in range(over):
                fdata[ii] = cdata[ii]*fdata[ii]
            invfft(tdata, 0, fdata, 0, 1)
            for ii in range(size):
                buf[offset + ii] = tdata[ii]*chirp[ii]
            offset += size
    return function

def fft(dst, src, size):
    "Performs Forward FFT from src into dst"
    _fourstep(size, -1)(dst, 0, src, 0, 1)

def ifft(dst, src, size):
    "Performs Inverse FFT from src into dst"
    _fourstep(size, +1)(dst, 0, src, 0, 1)

def _hfft(dst, src, rows, cols, sign):
    xform = _fourstep(cols, sign)
    offset = 0
    for ii in range(rows):
        xform(dst, offset, src, offset, 1)
        offset += cols

def hfft(dst, src, rows, cols):
    "Performs Horizontal Forward FFTs from src into dst"
    _hfft(dst, src, rows, cols, -1)

def hifft(dst, src, rows, cols):
    "Performs Horizontal Inverse FFTs from src into dst"
    _hfft(dst, src, rows, cols, +1)

_scratch = _ffi.new('double _Complex[]', 1024)
def _vfft(dst, src, rows, cols, sign):
    global _scratch
    if len(_scratch) < rows:
        _scratch = _ffi.new('double _Complex[]', rows)
    xform = _fourstep(rows, sign)
    for jj in range(cols):
        xform(_scratch, 0, src, jj, cols)
        for ii in range(rows):
            dst[ii*cols + jj] = _scratch[ii]

def vfft(dst, src, rows, cols):
    "Performs Vertical Forward FFTs from src into dst"
    _vfft(dst, src, rows, cols, -1)

def vifft(dst, src, rows, cols):
    "Performs Vertical Forward FFTs from src into dst"
    _vfft(dst, src, rows, cols, +1)

def fft2d(dst, src, rows, cols):
    "Not implemented yet" # XXX
    assert False
    
def ifft2d(dst, src, rows, cols):
    "Not implemented yet" # XXX
    assert False

def rfft(dst, src, size):
    "Not implemented yet" # XXX
    assert False

def rifft(dst, src, size):
    "Not implemented yet" # XXX
    assert False

if __name__ == '__main__':
    if 0:
        rows = 8*3
        cols = 2*5
        #src = _ffi.new('double _Complex[]', rows*cols)
        #dst = _ffi.new('double _Complex[]', rows*cols)
        src = [0]*rows*cols
        dst = [0]*rows*cols
        src[0*cols + 0] = complex(1, 0)
        src[1*cols + 1] = complex(1, 0)
        src[(rows/2)*cols + 2] = complex(1, 0)
        src[(rows/4)*cols + 3] = complex(1, 0)
        vfft(dst, src, rows, cols)
        for ii in range(rows):
            for jj in range(cols):
                zz = dst[ii*cols + jj]
                print "  %+.3f%+.3fj" % (zz.real, zz.imag),
            print

    if 0:
        size = 2*5*7
        src = _ffi.new('double _Complex[]', size)
        dst = _ffi.new('double _Complex[]', size)
        src[size/2] = 1
        fft(dst, src, size)
        for ii in range(size):
            print "\t%+.3f  %+.3fj" % (dst[ii].real, dst[ii].imag)

    if 0:
        import time
        size = 256*1024
        src = _ffi.new('double _Complex[]', size)
        dst = _ffi.new('double _Complex[]', size)
        mintime = 1e100
        for its in range(20):
            for ii in range(size):
                src[ii] = complex(0, 0)
            src[1] = 1
            before = time.time()
            fft(dst, src, size)
            duration = time.time() - before
            mintime = min(mintime, duration)
            print duration
        print "min time:", mintime
        
    if 0:
        for size in [128*1024, 256*1024, 3*5*7*13*29, 11025]:
            maxerr = 0
            print 'Testing FFT size:', size
            src = _ffi.new('double _Complex[]', size)
            mid = _ffi.new('double _Complex[]', size)
            dst = _ffi.new('double _Complex[]', size)
            src[3] = complex(1, 0)
            fft(mid, src, size)
            ifft(dst, mid, size)
            for ii in range(size):
                err = src[ii] - dst[ii]/size
                maxerr = max(maxerr, abs(err))
                assert abs(err) < 1e-12
            print "maxerr:", maxerr


