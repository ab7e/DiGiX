#
# Copyright 2020 Scott Eric Gilbert.  All Rights Reserved.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# This Source Code Form is "Incompatible With Secondary Licenses", as
# defined by the Mozilla Public License, v. 2.0.
#

from cffi import FFI
_winapi = FFI()
_winapi.cdef(r'''
    // At the time of writing this, PyPy only supports running as a 32 bit
    // process on Windows, but these definitions use size_t and ssize_t in 
    // such a way that they should be correct for 64 bit Windows too.

    typedef struct {
        uint16_t mid, pid;
        uint32_t version;
        char name[32];
        uint32_t formats;
        uint16_t channels;
        uint16_t reserved;
        uint32_t support;
    } WaveOutCaps;

    typedef struct {
        uint16_t mid, pid;
        uint32_t version;
        char name[32];
        uint32_t formats;
        uint16_t channels;
        uint16_t reserved;
    } WaveInCaps;

    typedef struct {
        uint16_t format;
        uint16_t channels;
        uint32_t samplerate;
        uint32_t byterate;
        uint16_t blockalign;
        uint16_t samplebits;
        uint16_t zeroextra;
    } WaveFormat;

    typedef struct {
        short* data;
        uint32_t bytelen;
        uint32_t bytesrec;
        size_t userdata;
        uint32_t flags;
        uint32_t loops;
        size_t next;
        size_t reserved;
    } WaveHeader;

    typedef struct {
        char tag0[4];
        uint32_t size0;
        char kind[4];
        char tag1[4];
        uint32_t size1;
        uint16_t format;
        uint16_t chans;
        uint32_t srate;
        uint32_t brate;
        uint16_t align;
        uint16_t bits;
        char tag2[4];
        uint32_t size2;
        int16_t data[];
    } WavFileFormat;

    WINAPI size_t GetModuleHandleA(size_t);
    WINAPI uint32_t waveOutGetNumDevs();
    WINAPI uint32_t waveOutGetDevCapsA(
        uint32_t dev, WaveOutCaps*, uint32_t
    );
    WINAPI uint32_t waveOutOpen(
        size_t*, int32_t, WaveFormat*,
        size_t, size_t, uint32_t
    );
    WINAPI uint32_t waveOutPrepareHeader(
        size_t, WaveHeader*, uint32_t
    );
    WINAPI uint32_t waveOutWrite(
        size_t, WaveHeader*, uint32_t
    );
    WINAPI uint32_t waveOutUnprepareHeader(
        size_t, WaveHeader*, uint32_t
    );
    WINAPI uint32_t waveOutClose(size_t);
    WINAPI uint32_t waveInGetNumDevs();
    WINAPI uint32_t waveInGetDevCapsA(
        uint32_t, WaveInCaps*, uint32_t
    );
    WINAPI uint32_t waveInOpen(
        size_t*, int32_t, WaveFormat*,
        size_t, size_t, uint32_t
    );
    WINAPI uint32_t waveInPrepareHeader(
        size_t, WaveHeader*, uint32_t
    );
    WINAPI uint32_t waveInAddBuffer(
        size_t, WaveHeader*, uint32_t
    );
    WINAPI uint32_t waveInStart(size_t);
    WINAPI uint32_t waveInUnprepareHeader(
        size_t, WaveHeader*, uint32_t
    );
    WINAPI uint32_t waveInStop(size_t);
    WINAPI uint32_t waveInClose(size_t);
''')

_kernel32 = _winapi.dlopen("kernel32.dll")
_winmm = _winapi.dlopen("winmm.dll")

_mapping = [
    # We're ignoring 8 bit modes and only supporting 16 bits
    (0x00004, (11025, 'mono')), (0x00008, (11025, 'stereo')),
    (0x00040, (22050, 'mono')), (0x00080, (22050, 'stereo')),
    (0x00400, (44100, 'mono')), (0x00800, (44100, 'stereo')),
    (0x04000, (48000, 'mono')), (0x08000, (48000, 'stereo')),
    (0x40000, (96000, 'mono')), (0x80000, (96000, 'stereo')),
]

def outdevs():
    "Returns a list of Output Device (name, (rate, mode))"
    count = _winmm.waveOutGetNumDevs()
    outcaps = _winapi.new('WaveOutCaps*')
    bytelen = _winapi.sizeof('WaveOutCaps')
    result = []
    for ii in range(count):
        _winmm.waveOutGetDevCapsA(ii, outcaps, bytelen)
        modes = []
        for mask, desc in _mapping:
            if outcaps.formats & mask:
                modes.append(desc)
        name = _winapi.string(outcaps.name)
        result.append((name, modes))
    return result

def indevs():
    "Returns a list of Input Device (name, (rate, mode))"
    count = _winmm.waveInGetNumDevs()
    incaps = _winapi.new('WaveInCaps*')
    bytelen = _winapi.sizeof('WaveInCaps')
    result = []
    for ii in range(count):
        _winmm.waveInGetDevCapsA(ii, incaps, bytelen)
        modes = []
        for mask, desc in _mapping:
            if incaps.formats & mask:
                modes.append(desc)
        name = _winapi.string(incaps.name)
        result.append((name, modes))
    return result

def play(data, rate, device=-1, stereo=False):
    '''
    Play PCM samples at the provided sample rate, optionally specifying the
    device and stereo.  The data samples should be 16 bit signed shorts.  The
    device is the index of the device from the list returned by outdevs().

        from math import cos, pi
        from time import sleep
        rate = 11025
        secs = 4.0
        data = _winapi.new('short[]', int(rate*secs + .5))
        for ii in range(len(data)):
            phase = 2*pi*ii*220/rate
            data[ii] = int(8000*cos(phase))
        play(data, rate, dev=0)
        sleep(secs)

    This function returns immediately and does not wait for all of the samples
    to be played.  If no device is specified, Windows will choose a default.

    The sample rate should probably be one of the ones given by the outdevs()
    function, but other rates (8000, 12000) will probably work.  There may be
    glitches from low quality resampling though.
    '''
    chans = 2 if stereo else 1
    ptr = _winapi.new('size_t*')
    fmt = _winapi.new('WaveFormat*')
    fmt.format = 1 # WAVE_FORMAT_PCM
    fmt.channels = chans
    fmt.samplerate = rate
    fmt.byterate = 2*chans*rate
    fmt.blockalign = 2*chans
    fmt.samplebits = 16
    res = _winmm.waveOutOpen(ptr, device, fmt, 0, 0, 0);
    assert res == 0, "waveOutOpen"
    hout = ptr[0]
    hdr = _winapi.new('WaveHeader*')
    hdr.data = data
    hdr.bytelen = 2*len(data)
    _winmm.waveOutPrepareHeader(
        hout, hdr, _winapi.sizeof('WaveHeader')
    )
    _winmm.waveOutWrite(
        hout, hdr, _winapi.sizeof('WaveHeader')
    )
    _winmm.waveOutUnprepareHeader(
        hout, hdr, _winapi.sizeof('WaveHeader')
    )
    _winmm.waveOutClose(hout)

class Recorder(object):
    def __init__(self, samples, rate, device=-1, buffers=4, stereo=False):
        '''
        Creates a Recorder object that reads samples from a soundcard.

            from time import sleep
            rate = 11025
            samples = 11025/5
            with Recorder(samples, rate, buffers=16) as rec:
                data = _winapi.new('short[]', samples)
                while 1:
                    if rec.poll(data):
                        play(data, rate)
                    else:
                        sleep(.01)
        '''
        chans = 2 if stereo else 1
        assert samples % chans == 0
        hinstance = _kernel32.GetModuleHandleA(0)
        ptr = _winapi.new('size_t*')
        fmt = _winapi.new('WaveFormat*')
        fmt.format = 1 # WAVE_FORMAT_PCM
        fmt.channels = chans
        fmt.samplerate = rate
        fmt.byterate = 2*chans*rate
        fmt.blockalign = 2*chans
        fmt.samplebits = 16
        res = _winmm.waveInOpen(ptr, device, fmt, 0, hinstance, 0)
        assert res == 0, 'waveInOpen'
        self.handle = ptr[0]
        self.buffer = []
        self.header = []
        self.index = 0
        hdrsize = _winapi.sizeof('WaveHeader')
        for bb in range(buffers):
            buf = _winapi.new('short[]', samples)
            hdr = _winapi.new('WaveHeader*')
            hdr.data = buf
            hdr.bytelen = 2*samples
            self.buffer.append(buf)
            self.header.append(hdr)
            _winmm.waveInPrepareHeader(self.handle, hdr, hdrsize)
            _winmm.waveInAddBuffer(self.handle, hdr, hdrsize)
        _winmm.waveInStart(self.handle)

    def poll(self, data):
        "Returns True if data was ready and copied into the samples"
        samples = len(data)
        assert samples == len(self.buffer[0])
        hdr = self.header[self.index]
        hdrsize = _winapi.sizeof('WaveHeader')
        res = _winmm.waveInUnprepareHeader(self.handle, hdr, hdrsize)
        if res != 33: # WAVERR_STILLPLAYING
            buf = self.buffer[self.index]
            for ii in range(samples):
                data[ii] = buf[ii]
            _winmm.waveInPrepareHeader(self.handle, hdr, hdrsize)
            _winmm.waveInAddBuffer(self.handle, hdr, hdrsize)
            self.index = (self.index + 1) % len(self.buffer)
            return True
        return False

    def close(self):
        _winmm.waveInStop(self.handle)
        _winmm.waveInClose(self.handle)

    def __enter__(self): return self
    def __exit__(self, *ignored): self.close()
    def __del__(self): self.close()


if __name__ == '__main__':
    if 0:
        from pprint import pprint
        pprint(outdevs())
        pprint(indevs())

    if 0:
        from time import sleep
        rate = 11025
        samples = 11025/5
        with Recorder(samples, rate, buffers=16) as rec:
            data = _winapi.new('short[]', samples)
            while 1:
                if rec.poll(data):
                    play(data, rate)
                else:
                    sleep(.01)

    if 0:
        from math import cos, pi
        from time import sleep
        rate = 11025
        secs = 4.0
        data = _winapi.new('short[]', int(rate*secs + .5))
        for ii in range(len(data)):
            phase = 2*pi*ii*220/rate
            data[ii] = int(8000*cos(phase))
        play(data, rate, dev=0)
        sleep(secs)


#
#
#
#
#def makewav(secs, rate, stereo=False):
#    '''
#    Builds a WAV object of signed 16 bit samples and fills in the header.
#    The data samples start off as all zeros and should be modified.
#
#    If creating stereo data, the even and odd samples are left and right
#    channels respectively.
#    '''
#    chans = 2 if stereo else 1
#    samples = int(secs*rate + .5)
#    result = _winapi.new(
#        'WavFileFormat*', { 'data': samples }
#    )
#    result.tag0   = 'RIFF'
#    result.size0  = 36 + 2*samples
#    result.kind   = 'WAVE'
#    result.tag1   = 'fmt '
#    result.size1  = 16
#    result.format = 1
#    result.chans  = chans
#    result.srate  = rate
#    result.brate  = 2*rate*chans
#    result.align  = 2*chans
#    result.bits   = 16
#    result.tag2   = 'data'
#    result.size2  = 2*samples
#    return result
#
#def play(wav, wait=True):
#    '''
#    Plays a WAV object.  See makewav() for details.
#    
#    If wait is False, it will return immediately.
#    '''
#    _winmm.PlaySoundA(wav, 0, 4 if wait else 5)
#
#
#if 0:
#    # Make a 4 second wave file
#    from math import cos, pi
#
#    rate = 11025
#    secs = 4.0
#    wav = makewav(secs, rate)
#
#    for ii in range(len(wav.data)):
#        phase = 2*pi*ii*160/rate
#        wav.data[ii] = int(8000*cos(phase))
#
#    play(wav)
#

