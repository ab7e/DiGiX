from math import log10
from cffi import FFI
from soundcard import Recorder, play
from graphics import Window, Pixmap
from fourier import fft

ffi = FFI()

def makepal():
    result = Pixmap(3*256, 1)
    for ii in range(256):
        result[0, ii      ].r = ii
        #result[0, 255 - ii].b = ii/2
        result[0, ii + 256].r = 255
        result[0, ii + 512].r = 255
        result[0, ii + 256].g = ii
        result[0, ii + 512].g = 255
        result[0, ii + 512].b = ii
    return result

def magsqr(zz):
    return zz.real*zz.real + zz.imag*zz.imag

def decibels(xx):
    return 10*log10(xx + 1e-20)

def rasterize(pix, spec, pal):
    scratch = ffi.new('double[]', len(spec)/2)
    minval = maxval = magsqr(spec[0])
    for ii in range(len(spec)/2):
        val = magsqr(spec[ii])
        minval = min(minval, val)
        maxval = max(maxval, val)
        scratch[ii] = val
    #mindb = decibels(minval)
    L = list(scratch)
    L.sort() # find quartile
    mindb = decibels(L[len(L)/4])
    maxdb = decibels(maxval) 
    palscale = (pal.width - 1)/(maxdb - mindb)
    specscale = (len(spec) - 1)/(2.0*pix.width)
    for ii in range(pix.width):
        total = 0.0
        for dd in range(10):
            xx = ii + dd*.1
            specspot = int(xx*specscale)
            total += scratch[specspot]
        db = decibels(total*.1)
        palspot = int((db - mindb)*palscale)
        palspot = max(0, palspot)
        pix[0, ii].c = pal[0, palspot].c

def main():
    width   = 1200
    height  = 800
    rate    = 11025
    fftsize = rate/5
    block   = ffi.new('short[]', fftsize)
    spec    = ffi.new('double _Complex[]', fftsize)
    palette = makepal()
    pix     = Pixmap(width, 1)
    with Window('Example', 1200, 800) as win:
        with Recorder(fftsize, rate) as rec:
            while True:
                evt = win.wait(.001)

                while rec.poll(block):
                    fft(spec, block, fftsize)
                    rasterize(pix, spec, palette)
                    win.copyrect(0, 0, 0, 1, width, height - 1)
                    win.drawpix(0, height - 1, pix)
                    win.bitblt()

                if evt.kind == 'close':
                    break

if __name__ == '__main__':
    main()

