#
# Copyright 2020 Scott Eric Gilbert.  All Rights Reserved.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# This Source Code Form is "Incompatible With Secondary Licenses", as
# defined by the Mozilla Public License, v. 2.0.
#

from cffi import FFI
from collections import deque
import time, math

_winapi = FFI()
_winapi.cdef(r'''
    // At the time of writing this, PyPy only supports running as a 32 bit
    // process on Windows, but these definitions use size_t and ssize_t in 
    // such a way that they should be correct for 64 bit Windows too.

    typedef union {
        ssize_t lparam;
        struct {
            int16_t loword;
            int16_t hiword;
        };
        //void* pointer;
    } LongParam;

    typedef WINAPI ssize_t (*WndProc)(
        size_t, uint32_t, ssize_t, ssize_t
    );

    typedef struct {
        uint32_t  style;
        WndProc   wndproc;
        int32_t   clsextra;
        int32_t   wndextra;
        size_t    hinstance;
        size_t    hicon;
        size_t    hcursor;
        size_t    hbackground;
        const char* menuname;
        const char* classname;
    } WndClass;

    typedef struct {
        size_t    hwnd;
        uint32_t  kind;
        ssize_t   wparam;
        ssize_t   lparam;
        uint32_t  time;
        int32_t   pointx;
        int32_t   pointy;
        uint32_t  unknown;
    } WndMessage;

    //typedef struct {
    //    uint32_t style;
    //    uint32_t exstyle;
    //    uint16_t itemlen;
    //    int16_t x, y, w, h;
    //    uint16_t zero[3];
    //    struct {
    //        uint32_t style;
    //        uint32_t exstyle;
    //        int16_t x, y, w, h;
    //        uint16_t ident;
    //        uint16_t ffff; // 0xFFFF
    //        uint16_t kind; // 0x80 - 0x85
    //        uint16_t zero[3];
    //    } items[5];
    //} WndDialog5;

    typedef struct {
        int32_t   left;
        int32_t   top;
        int32_t   right;
        int32_t   bottom;
    } WndRect;

    typedef struct {
        size_t    hdc;
        int       erase;
        WndRect   rect;
        int       restore;
        int       update;
        char      reserved[32];
    } WndPaint;

    typedef struct {
        uint32_t  headsize;
        int32_t   width;
        int32_t   height;
        int16_t   planes;
        int16_t   bitcount;
        uint32_t  compression;
        uint32_t  sizeimage;
        int32_t   xpels;
        int32_t   ypels;
        uint32_t  colorsused;
        uint32_t  important;
    } WndPixmap;

    typedef union {
        uint32_t c;
        struct {
            uint8_t b, g, r, a;
        };
    } WndPixel;

    WINAPI int32_t MessageBoxA(
        size_t, const char*, const char*, uint32_t
    );
    WINAPI size_t GetModuleHandleA(size_t);
    WINAPI uint16_t RegisterClassA(WndClass*);
    WINAPI size_t LoadCursorA(size_t, size_t);
    WINAPI size_t GetStockObject(int32_t);
    WINAPI int AdjustWindowRectEx(
        WndRect*, uint32_t, int, uint32_t
    );
    WINAPI size_t CreateWindowExA( 
        uint32_t, const char*, const char*, uint32_t, 
        uint32_t, uint32_t, int32_t, int32_t, 
        size_t, size_t, size_t, size_t
    );
    WINAPI int PeekMessageA(
        WndMessage*, size_t, uint32_t, uint32_t, uint32_t
    );
    WINAPI int GetMessageA(
        WndMessage*, size_t, uint32_t, uint32_t
    );
    WINAPI int TranslateMessage(WndMessage*);
    WINAPI ssize_t DispatchMessageA(WndMessage*);
    WINAPI ssize_t DefWindowProcA(
        size_t, uint32_t, ssize_t, ssize_t
    );
    //WINAPI int PostMessageA(
    //    size_t, uint32_t, ssize_t, ssize_t
    //);
    WINAPI size_t SetCapture(size_t);
    WINAPI int ReleaseCapture(void);
    WINAPI int DestroyWindow(size_t);
    WINAPI int UnregisterClassA(
        const char*, size_t
    );
    WINAPI int GetClientRect(size_t, WndRect*);
    WINAPI size_t GetDC(size_t);
    WINAPI int32_t ReleaseDC(size_t, size_t);
    WINAPI size_t CreateCompatibleDC(size_t);
    WINAPI int DeleteDC(size_t);
    WINAPI size_t CreateCompatibleBitmap(
        size_t, int32_t, int32_t
    );
    WINAPI size_t BeginPaint(size_t, WndPaint*);
    WINAPI int EndPaint(size_t, WndPaint*);
    WINAPI size_t SelectObject(size_t, size_t);
    WINAPI int DeleteObject(size_t);
    WINAPI int32_t FillRect(size_t, WndRect*, size_t);
    WINAPI int BitBlt(
        size_t, int32_t, int32_t, int32_t, int32_t,
        size_t, int32_t, int32_t, uint32_t
    );
    WINAPI int32_t StretchDIBits(
        size_t,
        int32_t, int32_t, int32_t, int32_t,
        int32_t, int32_t, int32_t, int32_t,
        WndPixel*, WndPixmap*, uint32_t, uint32_t
    );
    WINAPI size_t CreatePen(int, int, uint32_t);
    WINAPI int MoveToEx(size_t, int32_t, int32_t, size_t);
    WINAPI int LineTo(size_t, int32_t, int32_t);
''')

_kernel32 = _winapi.dlopen("kernel32.dll")
_user32   = _winapi.dlopen("user32.dll")
_gdi32    = _winapi.dlopen("gdi32.dll")

class Pixmap(object):
    '''
    Pixmaps are 2D arrays of pixels.  The four components of the
    pixels are stored as bytes in bgra order, although the a (alpha)
    component is ignored when drawing to a window.  Here are two
    examples showing how to set the pixels:

    pixmap = Pixmap(800, 600)

    # Setting individual components of pixels
    for ii in range(pixmap.height):
        for jj in range(pixmap.width):
            pixmap[ii, jj].r = ii%256
            pixmap[ii, jj].g = 0
            pixmap[ii, jj].b = jj%256

    # Setting all components at once
    red   = 0xFF0000
    green = 0x00FF00
    blue  = 0x0000FF
    for ii in range(pixmap.height):
        for jj in range(pixmap.width):
            if ii < jj - 10:
                pixmap[ii, jj].c = red
            elif ii > jj + 10:
                pixmap[ii, jj].c = green
            else:
                pixmap[ii, jj].c = blue
    '''
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.pixels = _winapi.new('WndPixel[]', width*height)
        self._header = _winapi.new('WndPixmap*')
        self._header.headsize = 40
        self._header.width = width
        self._header.height = -height
        self._header.planes = 1
        self._header.bitcount = 32

    def __getitem__(self, (ii, jj)):
        return self.pixels[ii*self.width + jj]

class Event(object):
    '''
    Event objects are returned from the window.wait() method.
    The event.kind attribute is used to indicate what kind of
    event was returned, and there are additional attributes
    as needed for each kind of event:

    User is resizing the window:
        event.kind   == 'resize'
        event.w      == <int new width of window>
        event.h      == <int new height of window>

    Mouse is hovering over the window:
        event.kind   == 'hover'
        event.x      == <x location of mouse>
        event.y      == <y location of mouse>

    User clicked in the window:
        event.kind   == 'clicked'
        event.button == <str 'left', 'right', or 'middle'>
        event.x      == <int x location of mouse>
        event.y      == <int y location of mouse>

    User is dragging the mouse:
        event.kind   == 'dragging'
        event.button == <str 'left', 'right', or 'middle'>
        event.x0     == <int starting x location of mouse>
        event.y0     == <int starting y location of mouse>
        event.x1     == <int current x location of mouse>
        event.y1     == <int current y location of mouse>

    User finished dragging the mouse:
        event.kind   == 'dragged'
        event.button == <str 'left', 'right', or 'middle'>
        event.x0     == <int starting x location of mouse>
        event.y0     == <int starting y location of mouse>
        event.x1     == <int final x location of mouse>
        event.y1     == <int final y location of mouse>

    User requested to close the window
        event.kind   =='close' 

    '''
    def __init__(self, kind, **kwds):
        self.kind = kind
        self.__dict__.update(kwds)

# This maps { hwnd : window }.  The normal way to do this
# is to use SetWindowLong[Ptr], but that is problematic when
# going through cffi.from_handle and unknown window handles.
_hwndmap = {}

# This maps Win32 virtual key (VK) codes to strings
_vkeymap = {
    0x21: 'page up',       0x22: 'page down',
    0x23: 'end',           0x24: 'home', 
    0x25: 'arrow left',    0x26: 'arrow up',
    0x27: 'arrow right',   0x28: 'arrow down',
    0x2D: 'insert',        0x2E: 'delete',
    0x90: 'num lock',      0x91: 'scroll lock',
    0x13: 'pause break',
    0x70: 'F1',  0x71: 'F2',   0x72: 'F3',   0x73: 'F4',
    0x74: 'F5',  0x75: 'F6',   0x76: 'F7',   0x77: 'F8',
    0x78: 'F9',  0x79: 'F10',  0x7A: 'F11',  0x7B: 'F12',
}

@_winapi.callback('WndProc')
def _wndproc(hwnd, msg, wparam, lparam):

    window = _hwndmap.get(hwnd)
    if window is not None:
        #print "msg:", hex(msg)
        if msg == 0x005: # WM_SIZE
            window._resize = True

        if msg == 0x010: # WM_CLOSE
            window._deque.append(Event('close'))
            return 0

        if msg == 0x00F: # WM_PAINT
            hdc = _user32.BeginPaint(window._hwnd, window._paint)
            window._bitblt(hdc)
            _user32.EndPaint(window._hwnd, window._paint)

        if msg == 0x102: # WM_CHAR
            window._deque.append(Event('keypress', key=chr(wparam)))

        if msg == 0x100: # WM_KEYDOWN
            key = _vkeymap.get(wparam)
            if key:
                window._deque.append(Event('keypress', key=key))

        if msg in [0x201, 0x207, 0x204]:
            # WM_LBUTTONDOWN WM_MBUTTONDOWN WM_RBUTTONDOWN
            if window._button == 0:
                window._param.lparam = lparam
                window._downx = window._param.loword
                window._downy = window._param.hiword
                window._button = msg + 1
                window._isdrag = False
                _user32.SetCapture(hwnd)
        
        if msg == 0x200: # WM_MOUSEMOVE
            window._param.lparam = lparam
            x = window._param.loword
            y = window._param.hiword
            if window._button:
                dx = x - window._downx
                dy = y - window._downy
                if dx*dx + dy*dy > 25: window._isdrag = True
                if window._isdrag:
                    if window._button == 0x202: button = 'left'
                    if window._button == 0x208: button = 'middle'
                    if window._button == 0x205: button = 'right'
                    event = Event(
                        'dragging',
                        button=button,
                        x0=window._downx,
                        y0=window._downy,
                        x1=x, y1=y
                    )
                    window._deque.append(event)
            else:
                window._deque.append(Event('hover', x=x, y=y))

        if msg in [0x202, 0x208, 0x205]:
            # WM_LBUTTONUP WM_MBUTTONUP WM_RBUTTONUP
            if window._button == msg:
                _user32.ReleaseCapture()
                window._button = 0
                window._param.lparam = lparam
                x = window._param.loword
                y = window._param.hiword
                if msg == 0x202: button = 'left'
                if msg == 0x208: button = 'middle'
                if msg == 0x205: button = 'right'
                if window._isdrag:
                    event = Event(
                        'dragged',
                        button=button,
                        x0=window._downx,
                        y0=window._downy,
                        x1=window._param.loword,
                        y1=window._param.hiword
                    )
                else:
                    event = Event(
                        'clicked',
                        button=button,
                        x=window._downx,
                        y=window._downy
                    )
                window._deque.append(event)

    return _user32.DefWindowProcA(hwnd, msg, wparam, lparam)

@_winapi.callback('WndProc')
def _dlgproc(hdlg, msg, wparam, lparam):
    if msg == 0x110: # WM_INITDIALOG
        param = _winapi.new('LongParam*')
        param.lparam = lparam
        print param.pointer
        pass

    if msg == 0x005: # WM_SIZE
        pass

    if msg == 0x111: # WM_COMMAND
        pass


class Window(object):
    '''
    This class creates a graphical window on the screen, and provides a simple
    set of methods for drawing lines and pixmaps.  The wait() method is called
    to retrieve user interface events such as mouse clicks or key presses.
    Please see the documentation for the Event class.
    
    The window maintains an offscreen image (double buffer) for drawing.  The
    drawpix() and copyrect() method work on this hidden "canvas" and are
    intended for high quality graphics.  After changes are made, the bitblt()
    function copies the background buffer to the foreground window.  This keeps
    the graphics from flickering, and allows the Window class to perform
    repainting operations automatically.

    The drawline(), drawrect(), and drawarrow() methods draw directly on the
    window itself, and are intended for "transient" animations such as drawing
    zoom rectangles, cross hairs, motion arrows, and so on.  This should
    generally only be used during the 'dragging' or 'hover' events, and only
    after calling bitblt() to clear previous transients.

    The on-screen size and position of the window is not controllable by this
    class.  A user may make the window as large or small as they want.
    However, the size of the background buffer is only changed by the
    programmer calling the resize() method.  When the resize() method changes
    the size, the contents are lost and should be redrawn.

    Here is a simple example using the Window class:

    with Window('Example', 800, 600) as win:
        pix = Pixmap(400, 300)
        for ii in range(pix.height):
            for jj in range(pix.width):
                pix[ii, jj].r = ii%256
                pix[ii, jj].b = jj%256
        win.drawpix(200, 150, pix)

        while True:
            evt = win.wait()

            if evt.kind == 'dragging':
                win.bitblt()
                if evt.button == 'left':
                    win.drawrect(evt.x0, evt.y0, evt.x1, evt.y1, 0xFFCC00, 3)
                if evt.button == 'right':
                    win.drawarrow(evt.x0, evt.y0, evt.x1, evt.y1, 0x00CCFF, 3)
                print 'dragging', evt.button, evt.x0, evt.y0, evt.x1, evt.y1

            if evt.kind == 'close':
                break

    Note that if you do not use the with statement to manage the window,
    you should use the Window.close() method when you are done to release
    the resources it allocated.
    '''

    def __init__(self, title, width, height):
        self._deque = deque()
        hinstance = _kernel32.GetModuleHandleA(0)
        self._wndclass = _winapi.new('WndClass*')
        self._wndclass.wndproc = _wndproc
        self._wndclass.hinstance = hinstance
        self._wndclass.hcursor = _user32.LoadCursorA(0, 32512) # IDC_ARROW
        self._wndclass.hbackground = _gdi32.GetStockObject(4) # BLACK_BRUSH
        self._classname = _winapi.new('char[]', "general")
        self._wndclass.classname = self._classname
        _user32.RegisterClassA(self._wndclass)

        self._message = _winapi.new('WndMessage*')
        self._paint = _winapi.new('WndPaint*')
        self._param = _winapi.new('LongParam*')
        self._rect = _winapi.new('WndRect*')

        self._rect.left = 100
        self._rect.top = 100
        self._rect.right = 100 + width
        self._rect.bottom = 100 + height
        style = 0x10CF0000 # WS_VISIBLE | WS_OVERLAPPEDWINDOW,
        _user32.AdjustWindowRectEx(self._rect, style, 0, 0)

        self._width = width
        self._height = height
        self._hwnd = _user32.CreateWindowExA(
            0, "general", title, style,
            0x80000000, 0x80000000, # CW_USEDEFAULT
            self._rect.right - self._rect.left,
            self._rect.bottom - self._rect.top,
            0, 0, hinstance, 0
        )
        _hwndmap[self._hwnd] = self
        self._hdcwin = _user32.GetDC(self._hwnd)
        self._hdcbmp = _gdi32.CreateCompatibleDC(self._hdcwin)
        hnewbmp = _gdi32.CreateCompatibleBitmap(self._hdcwin, width, height)
        self._holdbmp = _gdi32.SelectObject(self._hdcbmp, hnewbmp)
        self._resize = True
        self._button = 0
        self._isdrag = False
        self._downx = 0
        self._downy = 0
        #_user32.PostMessageA(self._hwnd, 0x400, 0, 0)

    def close(self):
        _hwndmap.pop(self._hwnd, self)
        hnewbmp = _gdi32.SelectObject(self._hdcbmp, self._holdbmp)
        _gdi32.DeleteObject(hnewbmp)
        _gdi32.DeleteDC(self._hdcbmp)
        _user32.ReleaseDC(self._hwnd, self._hdcwin)
        _user32.DestroyWindow(self._hwnd)
        hinstance = _kernel32.GetModuleHandleA(0)
        _user32.UnregisterClassA(self._classname, hinstance)

    def wait(self, timeout=float('inf')):
        deadline = time.time() + timeout
        while time.time() < deadline:
            while not len(self._deque):
                if not _user32.PeekMessageA(
                    self._message, self._hwnd,
                    0, 0, 0 # PM_NOREMOVE
                ): break
                _user32.GetMessageA(self._message, 0, 0, 0)
                _user32.TranslateMessage(self._message)
                _user32.DispatchMessageA(self._message)

            if len(self._deque):
                return self._deque.popleft()

            if self._resize:
                self._resize = False
                _user32.GetClientRect(self._hwnd, self._rect)
                w=self._rect.right - self._rect.left
                h=self._rect.bottom - self._rect.top
                if w > 0 and h > 0: return Event('resize', w=w, h=h)

            time.sleep(.01)
        return Event('timeout')

    def resize(self, w, h):
        if self._width != w or self._height != h:
            hnewbmp = _gdi32.CreateCompatibleBitmap(self._hdcwin, w, h)
            holdbmp = _gdi32.SelectObject(self._hdcbmp, hnewbmp)
            _gdi32.DeleteObject(holdbmp)
            self._width = w
            self._height = h

    def drawpix(self, x, y, pixmap):
        _gdi32.StretchDIBits(
            self._hdcbmp,
            x, y, pixmap.width, pixmap.height,
            0, 0, pixmap.width, pixmap.height,
            pixmap.pixels, pixmap._header,
            0, 0xCC0020 # SRCCOPY
        )

    def _bitblt(self, hdc):
        _gdi32.BitBlt(
            hdc, 0, 0, self._width, self._height,
            self._hdcbmp, 0, 0, 0xCC0020 # SRCCOPY
        )
        hbrush = _gdi32.GetStockObject(4) # BLACK_BRUSH
        self._rect.top = 0
        self._rect.left = self._width
        self._rect.bottom = 100000
        self._rect.right = 100000
        _user32.FillRect(hdc, self._rect, hbrush)
        self._rect.top = self._height
        self._rect.left = 0
        _user32.FillRect(hdc, self._rect, hbrush)

    def bitblt(self):
        self._bitblt(self._hdcwin)

    def _swaprgb(self, rgb):
        r = (rgb >>  0)&0xFF
        g = (rgb >>  8)&0xFF
        b = (rgb >> 16)&0xFF
        return (r<<16) | (g<<8) | b

    def drawline(self, x0, y0, x1, y1, c, w=0):
        hnewpen = _gdi32.CreatePen(0, w, self._swaprgb(c))
        holdpen = _gdi32.SelectObject(self._hdcwin, hnewpen)
        _gdi32.MoveToEx(self._hdcwin, x0, y0, 0)
        _gdi32.LineTo(self._hdcwin, x1, y1)
        _gdi32.SelectObject(self._hdcwin, holdpen)
        _gdi32.DeleteObject(hnewpen)

    def drawrect(self, x0, y0, x1, y1, c, w=0):
        hnewpen = _gdi32.CreatePen(0, w, self._swaprgb(c))
        holdpen = _gdi32.SelectObject(self._hdcwin, hnewpen)
        _gdi32.MoveToEx(self._hdcwin, x0, y0, 0)
        _gdi32.LineTo(self._hdcwin, x0, y1)
        _gdi32.LineTo(self._hdcwin, x1, y1)
        _gdi32.LineTo(self._hdcwin, x1, y0)
        _gdi32.LineTo(self._hdcwin, x0, y0)
        _gdi32.SelectObject(self._hdcwin, holdpen)
        _gdi32.DeleteObject(hnewpen)

    def drawarrow(self, x0, y0, x1, y1, c, w=0, l=10):
        dx = x1 - x0
        dy = y1 - y0
        rr = l/math.hypot(dx, dy)
        dx = int(rr*dx)
        dy = int(rr*dy)
        hnewpen = _gdi32.CreatePen(0, w, self._swaprgb(c))
        holdpen = _gdi32.SelectObject(self._hdcwin, hnewpen)
        _gdi32.MoveToEx(self._hdcwin, x0, y0, 0)
        _gdi32.LineTo(self._hdcwin, x1, y1)
        _gdi32.MoveToEx(self._hdcwin, x1 - dy - dx, y1 + dx - dy, 0)
        _gdi32.LineTo(self._hdcwin, x1, y1)
        _gdi32.LineTo(self._hdcwin, x1 - dx + dy, y1 - dx - dy)
        _gdi32.SelectObject(self._hdcwin, holdpen)
        _gdi32.DeleteObject(hnewpen)

    def copyrect(self, dstx, dsty, srcx, srcy, w, h):
        _gdi32.BitBlt(
            self._hdcbmp, dstx, dsty, w, h,
            self._hdcbmp, srcx, srcy, 0xCC0020 # SRCCOPY
        )

    def __enter__(self): return self
    def __exit__(self, *ignored): self.close()
    def __del__(self): self.close()


if __name__ == '__main__':
    with Window('Example', 1200, 1000) as win:
        width, height = 1024, 1024
        pixmap = Pixmap(width, height)
        for ii in range(pixmap.height):
            for jj in range(pixmap.width):
                pixmap[ii, jj].r = ii%256
                pixmap[ii, jj].b = jj%256
        while True:
            evt = win.wait(1e-6)

            if evt.kind == 'timeout':
                win.copyrect(0, 0, 0, 1, width, height - 1)
                win.bitblt()

            if evt.kind == 'resize':
                print "resize", evt.w, evt.h
                width, height = evt.w, evt.h
                win.resize(width, height)

            if evt.kind == 'keypress':
                print "keypress", repr(evt.key)

            if evt.kind == 'hover':
                for ii in range(pixmap.height):
                    for jj in range(pixmap.width):
                        pixmap[ii, jj].r = (ii + evt.x)%256
                        pixmap[ii, jj].b = (jj + evt.y)%256
                win.drawpix(evt.x - 512, evt.y - 512, pixmap)
                win.bitblt()

            if evt.kind == 'clicked':
                print "click", evt.button, evt.x, evt.y

            if evt.kind == 'dragging':
                win.bitblt()
                if evt.button == 'left':
                    win.drawrect(evt.x0, evt.y0, evt.x1, evt.y1, 0xFFCC00, 3)
                if evt.button == 'right':
                    win.drawarrow(evt.x0, evt.y0, evt.x1, evt.y1, 0x00CCFF, 3)

            if evt.kind == 'dragged':
                print 'dragged', evt.button, evt.x0, evt.y0, evt.x1, evt.y1

            if evt.kind == 'close':
                break

